[PageOutline](PageOutline "wikilink")

## A short introduction to the notorious MxA bunch

MUAs, MTAs and other terms are used by email professionals to help
specify with precision what they are talking about.

Here are a few notes on what that means.

Mail handling is divided into three or more parts, depending on what
level of detail you want to go into. There's the client - a mail user
agent (MUA), the server that sends and queues mail - the mail transport
agent (MTA), retrieval from remote mailboxes (MRA), local delivery and
rewriting of messages - the mail delivery agent (MDA), and the remote
submission of messages (MSA).

The terms more precisely describe functions than describe software as
such. Sometimes a single program such as Mozilla Thunderbird or
Microsoft Outlook can combine multiple functions, in this case client,
retrieval, and processing rules. Sometimes they're separated out into
individual tools.

They are terms of convenience despite their confusing appearance. Exact
meanings can change with the context in which they are used.

For example, a unix machine that has its own hostname and MX records, so
that it can send and receive mail, there are only two different pieces
of software needed: Mutt as MUA, and sendmail as MTA which also has the
roles of MDA and MSA . The roles may be combined and are often not split
out.

**MUA**  
    Mail User Agent is a 'mail client program', the part you see
    and use to send and receive mail.

**MTA**  
    Mail Transfer Agent is anything between MUAs.  MTAs generally
    do the routing and transferring of mail between hosts, ISPs, and
    so on, depending on how detailed one wants to be.

**MDA**  
    Mail Delivery Agent. An email arrives and does not need to go
    further.  The MTA gives it to the Mail Delivery Agent, and the MDA
    puts it in an *incoming* mailbox. It might call other tools like
    procmail that then become part of its function.

**MSA**  
    On an ISP with lots of users that use MUAs on machines that
    don't have their own MTA, it is not enough to simply let all those
    users submit their messages to the MTA systems without any
    authentication.  Otherwise spammers will exploit this and abuse
    the system.  Accepting emails from users for sending elsewhere
    becomes a specialized task, and an MTA that does that is called
    the MSA.

**MRA**  
    A Mail Retrieval Agent makes connections to a
    remote mailbox and fetches mail for local use. Fetchmail as a
    standalone program is a common example.

Mutt is an email client, and thus a MUA. Its POP\&IMAP support means it
has MRA functionality as well.

A typical generic path of your Email through the Internet and on your
computer system might look as shown in these charts. Your setup may vary
in detail.

|| [MailConcept/Flow](MailConcept/Flow) || [MailConcept/Layout](MailConcept/Layout) ||

## MUA: Mail User Agent (email client)

The MUA is the front-end for the end user to manage and read stored
email into the user's mailbox(es) and compose new email to be sent out
via an MSA. This includes things like decoding MIME-encoded and
encrypted messages. [MuttTools](MuttTools) can help enhance mutt beyond default
built-in functionality.

Other popular MUAs include Thunderbird, Kmail, evolution, Sylpheed,
mulberry, pegasus, pine, and elm (mutt's predecessor)...

## MTA: Mail Transport Agent (SMTP server)

The MTA queues, receives from, and sends mail to other MTAs. This
includes routing mail, queuing and retrying, sending if the next MTA
doesn't accept email immediately, or sending a notification back to the
original sender if the next MTA cannot handle the message. Reasons might
include such things as the receiving mailbox does not exist, is full or
unavailable, or that it doesn't trust the sending MTA not to send spam.

On the internet, MTAs between themselves communicate via SMTP, the
Simple Mail Transfer Protocol. Some popular MTAs include:

* <http://www.exim.org/>  
* <http://www.postfix.org/>  
* <http://www.sendmail.org/>  
* <http://www.qmail.org/top.html>

In addition there are *small MTAs* ("SMTP clients" as opposed to fully
fledged "SMTP servers", above) that are targeted to perform a few
specific functions, without the overhead of the resources to run a
full-blown SMTP server. Such a program might not listen for incoming
messages and only run when there is something to send; typically it can
only pass messages to other MTAs without queuing, routing, or modifying
anything. See [SendmailAgents](SendmailAgents) for favorites.

An important reason for these difference is that SMTP requires MTAs to
take responsibility for handling the message when it accepts it. After
receiving all the data it reports successful acceptance of the message,
Acceptance includes taking responsibility for not subsequently losing
the message. If after trying several times it cannot deliver the message
it will normally report this back to the sender via a *bounce message*.
"SMTP servers" have to support queuing; "SMTP clients" do not, as this
has to be handled in a more complex and reliable way. In the case of a
MUA implementing "SMTP client" functionality, it usually can provide an
outbox of sorts to make up for the lack of queuing support.

### mutt and SMTP

**mutt can't speak SMTP** ... wait, it can ...  
... or why mutt should not but slowly becomes an "all-in-1" program.

mutt *was* developed with the concept of "one task per tool", enabling
performance through combination with other high quality modular
programs. This is an aspect of the "unix philosophy".

Putting too much into an application wastes unused resources --either in
storage (HDD/RAM) or in run-time performance-- when in many cases people
don't even use this extra functionality, or it is covered by some other
specialist tool. (With a commercial program you'd even have to pay for
the unused code, would you do that if you had the choice?)

That's why --so far-- things have been kept separate (or out of mutt).

For quite some time **basic** MRA functionality has been included in
mutt, which some consider a breach of the concept and therefore demand
inclusion of SMTP support as well (and because "all the other MUAs have
SMTP as well"). The original reasons for the MRA part are not recorded
(*ed: anyone who knows shall fill in*), but it makes sense to have MRA
in mutt and not
MTA:

* MUA+MRA access&store msgs **within** your control-reach (pull),  
* MTA passes msgs **beyond** your control boundary (push).

On this abstract level, MUA+MRA combined cover 1 area so they can both
go into one application like mutt, while MTA covers another. IMAP
already works with a folder system, so it's a simple step to include
that into mutt, and with the URL-syntax POP can be treated like a remote
folder, too.

"This entire concept is rubbish and the mutt developers are just plain
lazy \[to add all the good stuff\]."

Well, it seems the force of the masses is hard to withstand though ...
recently experimental minimal SMTP support has been added to mutt as an
option. If you fail to use a separate local MTA with mutt, try this
failsafe. But still the preferred way is the MTA way. Choose your own
optimal way for (multi-account) maintenance.

## MDA: Mail Delivery Agent

The MDA accepts a piece of mail from an MTA and performs the actual
delivery (including processing aka "filtering") to the user's mail
folders (see [FolderFormat](FolderFormat)). Most MTAs come with a simple MDA (e.g.
sendmail's mail.local). Some popular MDAs include:

* <http://www.procmail.org/>  
* maildrop

Some typical uses for **filtering**
include:

* sorting into multiple incoming folders.  
* pre/post-processing to "fix" or improve display/ management (like attachments).  
* automatic replies (but be careful not to respond to spammers)  
* Spam-defense: static black-/white-list, dynamic like spam-assassin (URL?), and other examples (URLs?).  
* Virus-checking: but that should be better done already at MTA level.  
* ...

## MRA: Mail Retrieval Agent (POP/IMAP client)

If you're lucky, then you might be already logged into the mail-server
with a cmd-shell to read your mail with mutt. If you're less lucky, then
you might be allowed to login remotely via ssh and start mutt on a
mail-server cmd-shell. In both cases you would not need any MRA, because
you could access your inbox directly off the mail-server's local file
system, but both cases are rare. Typically your mail is stored on a
mail-server which is different from the machine where you want to read
your mail, then you have to get your mail transferred somehow.

In organizations this sometimes is provided via NFS, which means the
remote mail-server filesystem appears on your local machine as if it
were local, too (keyword "mount"). As end-user normally you don't notice
a difference, and likewise there is no need for you to configure
anything with your MUA.

As regular private user (or even in organized environments) you
typically have to *download* (and can even store back) your mail via
IMAP or POP by means of an MRA to store & access it locally. For simple/
casual use, MRA functionality has been implemented into mutt (like most
MUAs do), but only in so far as to allow to speak POP + IMAP. Mutt
treats POP-/IMAP-access like local folders when you use the URL-syntax
(see [MuttGuide/Folders](MuttGuide/Folders)). However, mutt does not process any messages
through an MDA! Only real MRA do this.

For frequent/ regular use or when MDA functionality on the mail-server
is limited, it's recommended to employ a dedicated MRA to manage the
transfer automatically and to allow for post-processing and local
delivery. Examples are:

* fetchmail (http://www.fetchmail.info/)  
* getmail (http://pyropus.ca/software/getmail/)  
* retchmail (http://freecode.com/projects/retchmail)

-----

## Note

Those are by far not the only possible schemes, many variations exist.

For example the POP/ IMAP retrieval program may directly drop emails in
the user mailbox (acting as an MDA) or may transmit the incoming mails
directly to a dedicated mail delivery program, skipping the local mail
server at all (a good idea in a system without a local server).

The local server could directly append the incoming messages to the user
mailbox, skipping completely a dedicated mail processing agent, acting
as a delivering agent.

Finally the MUA could directly connect itself to the local or remote
server (as most MUAs do) acting as an SMTP client and it may fetch the
mails directly from a remote POP/ IMAP server (as mutt **can** actually
do) skipping entirely the whole retrieval program, local mail server,
mail processing/delivery program chain.

-----

see also:

* <http://www.iki.fi/era/procmail/mini-faq.html#appendix-mx>  
* <http://www.feep.net/sendmail/tutorial/intro/MUA-MTA-MDA.html>  
* <http://ebusiness.gbdirect.co.uk/howtos/mail-system.html>  
* <http://twiki.org/cgi-bin/view/Wikilearn/EmailServerSketches>  
* <http://www.tldp.org/HOWTO/Mail-User-HOWTO/>  
* <http://www.netbsd.org/docs/guide/en/chap-mail.html>
