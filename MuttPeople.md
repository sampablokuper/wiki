When you do something to support mutt, add yourself (or add others
**only with their agreement**!!!) so people know who to bother when
something goes wrong or needs to be done, or an expert opinion is due.
Use a unique identifier well known among mutters, no need for full name
or eMail (but if email, then "role"-email rather than "personal", in
case the person changes).

-----

## Services

* www: Jeremy Blosser  
* ftp: Brendan Cully  
* dns: Steve Kennedy  
* releases: brendan  
* bts: brendan (admin), anyone else signing up + doing some work  
* wiki == editable by all, admin brendan, editor RadoQ  
* lists: Steve K., RadoQ  
* irc: "me" (scandal), RadoQ + several [MuttChannel](MuttChannel) people.

### Distro maintainers

* Debian: Myon (Christoph Berg) myon@debian.org
* Foresight Linux: Jonathan Smith  
* FreeBSD: Udo Schweigert  
* Gentoo: Fabian Groffen grobian@gentoo.org
* !OpenMandriva: tbd
* Momonga-linux: Tamo  
* NetBSD: Stable: Matthias Scheler [info](ftp://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/mail/mutt/README.html); Devel: Antoine Reilles [info](ftp://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/mail/mutt-devel/README.html)  
* OpenBSD: Stuart Henderson sthen@openbsd.org  
* !OpenSuse: Dr. Werner Fink werner@suse.com  
* Redhat / Fedora: Jan Pacner jpacner@redhat.com
* Slackware: tbd
* UNIX Packages / Sunfreeware: Colin Prior colin@unixpackages.com
## Docs

* ManualTOC (rewrite): RadoQ  
* [VarNames](VarNames): RadoQ

## Translations

* Japanese: Tamo

## 3rd party support/ services

* muttrc.vim: Kyle Wheeler

## Code

**ALL code requests** should be sent to the mutt-dev mailing list, see
[MuttLists](MuttLists). People listed here are just for keeping track of specific
area expertise. For 3rd party patches see [PatchList](PatchList).

### Committers

The following people have push rights to the official Mercurial
repository:

* Brendan Cully (irc: *brendan*)  
* Michael Elkins (*scandal*)  
* Thomas Roessler  
* Kevin !McCarthy (*kevin8t8*)  
* David Champion (*dgc*)

### Maintenance

* build system: dev-team  
* docs: dev-team

### Core parts

* imap: brendan  
* pop: dev-team  
* smtp: brendan  
* header cache: dev-team  
* body cache: dev-team  
* crypto (traditional): dev-team  
* crypto (gpgme): dev-team  
* idn: dev-team  
* iconv: dev-team

### Current features

* format=flowed: dev-team

### Future features

* !PluginSupport: Michael Elkins
