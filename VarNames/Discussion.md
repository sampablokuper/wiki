[PageOutline](PageOutline "wikilink")

## Add pro/contra arguments as you see fit

### Read them first before you add: avoid duplicates!

And keep it simple!

## Pro: why do it at all

### easier support (do-it-yourself + helpers)

Easier use -\> more popularity, might gain even more momentum for
development.

-----

Good software == code + docs.   
 **Improving software**
means not only improving features, but also improving their usability
for newbies.

-----

**The pain of "dev" users due to "alternates + envelope\_from" is still
going to hit "stable" users!**   
 Think of how the
"regular" users will react, when already the "smart" ones using "dev"
version complain for "such small changes".

* Why force them to go through **the same
hell**?  
* Why not **instead** ease the pain of those offering a one-action script to transform their configs **easily,
automatically**  
  instead of forcing them to go through it *manually and
for at least 2 cases: alternates +
use_envelope_from*?  
* When you **already have such a one-action
script**, changing _any_ number of vars means **_no extra_
pain.**

And make **BIG WARNINGS/ ANNOUNCEMENTS**.

Considering the changes since 1.4.x call the next official release
**"mutt 2.0"**, kind of "relaunch" (instead of muttng. ;-). This
possibly can add to the "note this" factor (maybe along with hitting
news magazines like heise.de).

-----

**"It might have been a good idea 8y ago", well, it \_STILL\_ is a good
idea!**

* Even though established configs would have to change, time doesn't change the idea's usefulness for new users.  
* There will always be **more new users than old
configs**, afterall every oldie has to be a newbie once.  
* There will **always** be new users, while **now** the *current* old users will die out over time.  
 * It's **NOT a lifetime job to
adapt** the changes for users, it's a one-time event and it will be (relatively) simple.  
 * But it will be a mutt's lifetime benefit!

-----

**If it changed would you stop upgrading or even using mutt?**

Since no code is changed, mutt still keeps its features! Once this
single change has passed, all will be fine again.   

**BUT** newbies and/or casual users who don't find the stuff they are
looking for might give up on mutt.

-----

**Balancing numbers of affected users:** Too many old users would have
to change their old config.

Let's see how many people are affected in which way:

* various types of **(many) manual
users**:  
 * casual, read all, read never (because meaningful names examples enough), read single/ few items.  
 * **ever growing** number of newbies.  
* many **_unheard_ STABLE** users **still facing
upgrade** with "alternates" + "envelope_from",  
 * who'll certainly **"complain"** as much as if not more than the "dev" users already,  
 * which could be **helped** with this /Script, and then number of changes doesn't matter.

**VS.**

* a **fixed** number of one-time changers ("dev" users) who should be able to cope with it given **announcement** and [VarNames/Script](VarNames/Script).  
* unskilled users **with** customized config **not** containing "alternates" nor "use_envelope_from".

Those can't be expressed in numeric values, but compared to only the
**\_fixed\_ current** numbers, they will outgrow the latter. Ultimately
the **everlasting growth** of newbies having a useful manual **plus all
the stable users to UPGRADE NOW** wins by length against the **current**
number of **"dev" users and unaffected by the 2 vars stable users**,
who'd be the only ones to make an **extra change**.

Most **"stable"** users have to change anyway!!! And those "dev"
users are "mature" enough to apply an update once, as they already had
to be for "alternates+envelope\_from".

That's the **PURPOSE of "dev"**: to be spearhead for changes! That's
their risk to have to do more work than stable users and to learn **what
can save stable users** so they don't have to go through the same pains.
"dev" user aren't allowed to complain now to "have to do more" than
"stable users", it's their "job".

-----

-----

## Contra: why not to

### It hurts too much ... really?

**It's too much pain. The balance is about the cost to \*every\*
deployed installation.**   
**You basically force work
on users by being non-compatible.**   
**Changing a lot
of stuff pisses people of.** (whole config)

tlr (Thomas Roessler the mutt maintainer) referenced the "alternates"
case: *I already get hate mail about the incompatible change to
alternates. Don't even think of changing ALL variable names. ;)*
  
*In the case of alternates, I was convinced that the
benefit was worth it.*

* It was/ is, it just caught many people **unprepared, by
surprise**!  
* Improving support for future users doesn't come without changes, and changes not without some **collective
effort and personal commitment/ sacrifice**.  
* There is only a **single
action** involved to transform a given config with the help of the /Script.  
* See above in "PRO" section, point "good idea": **stable** users are **still
going to
suffer** the same as **dev** users already have:  
 * learn from this, don't let them go through the same hell!

-----

**Global renaming just to make it prettier, I don't buy it.**
  
 **Rename doesn't help, people must rtfm anyway.
Better improve docs
instead.**

* This **is** improvement of docs since variable names are part of the manual and orientation within it!  
* **Experience says it does
help**! Gained from IRC [MuttChannel](MuttChannel), on mailing-list mutt-users and the already received feedback to this poll.  
* **There ARE users that
benefit**. So while maybe you **personally** don't see the benefit for yourself (since you're a veteran user), it still exists for others!  
 * Varames are **important** for newbies _not_ reading (and not required to read!) the whole manual to find a quick/ simple/ small solution.  
* It's not about the looks, but the usability: **do you
require users to read the f*** manual
EACH TIME for each SINGLE SMALL ITEM**?  
 * When the manual is **too
big** (like mutt's), it helps finding stuff if you can limit the search range to a "related" area (or exclude).  
 * The names themselves might help already instead of reading desc. of each single feature in hopes it might have a connection to the original problem.  
* It helps understanding examples **without having to
lookup in
manual** to get an idea how they affect a given part!  
* Meaningful names help **getting an idea where to
look at all** before you can RTFM.

-----

**In short, the benefit of changing names is MINIMAL. Who benefits from
it?**   
 **Varnames you are used to you remember,
others don't matter.**   
 **muttrcbuilder + good docs
is enough, names don't matter.**

* **WRONG**! While it might be true to you, this does **not
apply to everybody
else**!  
 * If you are not a newbie yourself, what is the basis for this judgement?  
* If this _is_ true to you, then the only thing you FEAR is applying the /Script once?  
 * A good config tool (whether internal or external) doesn't release you of a **good
_reference_
manual** for those not matching _your_ user-profile.  
* **The benefit is not for those who
don't use the manual
anyway:**  
 * Those who have 100% memory like you who can remember (_all_?) varnames.  
 * Those who have a fixed config and never change _ANYTHING_.  
* **This benefit is for all those who
_must_ use the
manual:**  
 * NEWBIES/ first contact users.  
 * Casual users, who incrementally develop config.  
 * Single issue problems (few things of the mostly useful defaults need to be changed): **it
is a reference manual**, dammit! ;-)  
 * Veteran users pointing newbies what to RTFM.

-----

**It's not worth the trouble for all the old
users**.

* "alternates" + "envelope_from" must be changed for **stable** users anyway, already breaking configs, examples and docs!  
 * **When they have to be fixed
anyway**, then why not do **all the stuff
right!?** Some current names just aren't intuitive.  
* Instead of refusing it "in the name of the current users", **let
them decide for
themselves**.  
 * You are not their representant: while you might not want to take the trade-off personally, **they
might consider it not too painful!**  
 * Don't vote on their behalf! **Give them your reasons
on here this
page**, not your vote! Those others **are** considered by the reasons!

**NOTE:** the goal of this poll is not to figure out whether it helps or
not or how much it helps them others!   
 The goal is
to figure out whether **YOU** personally will **accept** the one-action
**change**.   
 **Just for yourself alone and not
anybody else: don't
second-guess!**

* **Second-guessing** the outcome before the actual results to support a **phantom
majority** spoils the purpose of this inquiry:  
 * If **all oldies for
themselves** accepted this for ONCE, why vote **against
them in their name?**

-----

**Longer var names intimidate users looking at examples and make them
harder to remember.**

* Does it hurt more to see names, which are increased **just by
a single
word**, or does it help more when you have a better grasp of what it does?  
* Is it harder to remember a 1 or 2-word longer name than a **cryptic** one, where the meaning is not obvious?

-----

**Prominent announcements are missed for automatic updates.**
  
**Automatic updates will fail.**
  
*example: apt-get dist-upgrade, and there goes your
configuration? No good!*

* This will **already** happen **without the naming
scheme
anyway** thanks to the already approved change for "alternates"!!!  
* There are **"post-install" scripts** for exactly **this
purpose**, so the /Script can cover up the **hard syntax
break** by "alternates".  
* **Maintainers (distros and sysadmin) are
required to keep
track** of NEWS or CHANGES with BIG WARNING signs with any upgrade,  
* so that they in turn can then inform their users about conflicts within their own distro info system and advise them how to fix.

-----

**"Please run /usr/share/doc/mutt/fix-my-configuraiton." And btw, your
configuration isn't portable anywhere.**

* **Config already is not portable
anymore** thanks to "alternates"!!!  
* Changing all vars with a /Script is less work than 2 manually.

-----

**You invalidate a lot of existing knowledge that is out there on the
net, guides + examples.**

* The knowledge is not lost, it can be **collected, corrected
and offered at a central place like
mutt.org**.  
 * This is where people come to look first anyway, no?  
* **They already are
wrong** for "alternates" and "use_envelope_from" and some more other changes, because they aren't updated:  
 * "guides" are well advised to be updated already anyway, and when you have to go through, you can aswell do it for all (per script).  
* Maintenance means updating from time to time: unmaintained systems are quickly outdated anyway.  
* Preventing a "good idea" in favour of already outdated and unmaintained examples doesn't sound like a "good idea".

-----

**Before doing a fork, you could try to discuss the proposed change on
mutt-dev and/or mutt-users and use that as an
indicator.**

* This is what these pages are for, but a fork is missing the point: it's a **documentation** problem, **not** a **code** problem!  
* If the change doesn't apply to the **"official"
docs**, people won't use any fork just to have "pretty" names **while
all the main docs+examples about it don't
apply**.  
* It has to be a collective **_official_
effort** to bring the desired benefit, because that's **where
people look for docs
first**: the official mutt.org!  
* It's not about creating a new product and/or community, it's about **improving
the support for the existing one**.

-----

**Don't change all at once, make small steps ("by
erosion").**

* This task must be done all or nothing for a categorized and sorted list to be of use for newbs to make their way on their own.  
 * It must be complete, whole at once: step by step makes no sense.  
* It would require too much patience and awareness by too many people to stay in line with the new naming scheme when it takes too long.  
 * That won't work out, people will "mess" around again, never to reach an end.  
 * This would rather mean pain with no end. The other way it's truely a big pain (or not as big with script), but it's over after one step.

-----

**There would have to be synonyms.**   
*These make
configurations less understandable (because there are multiple ways to
express things), not more understandable.*

* But **only the documented names are the
authorative (supported)
ones** to rely on, that's what docs are for afterall.  
* Synonyms for a transition are just that: until they expire, **even
CURRENTLY USED synonyms are NOT documented
at
all!**  
* No more synonyms, no more confusion: "Hey, this var XYZ doesn't work?!" => "It's dead, Jim".

-----

**Must keep compatibility for long transition time.**
  
 *Yet still all synonyms must be documented so people
know what
changed.*

* Period need not be too long: introduce now with next release, old stuff gone by release thereafter:  
 * the last release step can be deployed faster than from 1.4 to the next release if needed to get rid of synonyms support faster.  
* A simple list of what is translated to what should suffice, no?  
 * With a script that should be easily done (or even look at the script itself, it will be there anyway).  
* BUT **why would a long compatibility time
hurt?**  
 * **There are already
ANCIENT** synonyms about which nobody complains, neither being used in configs nor being undocumented!

-----

**A number of mutt users will struggle to migrate their config!!!**
  
 **I'm sorry, but I don't expect any
sed/awk/perl/logo script to work seamlessley for all.**
  
**Bear in mind that not all Mutt users are technical
and/or subscribe to mailing lists and
newsgroups.**

* True, there is the point that admins will have to take care of their users who don't do tech-stuff themselves.  
* But if they truly are not so much into "hacking", then they probably don't have configs that would fail with the /Script (**it
works, just try**!),  
 * which the admin could run for them once or they themselves.

-----

**There are other MORE important things TO DO ABOUT THE MANUAL (like
sane defaults and
other).**

* Well, if there are several things about the manual that need fixing:  
 * why not go through all of it and **APPLY _ALL_
IMPROVEMENTS
NOW** before the next release?  
* If there are going to be several significant changes that could cause confusion (which there are already!),  
 * let's rather have a big one (cut) instead of continously confusing (cutting) people.

-----

**init.h grows for synonyms, ugly to look at/ work
with.**

* Include extra file for synonyms (#include synonyms.h). Easy to get rid of once the transition time is declared over.  
* They aren't used for anything else and they are not mentionend in the docs, so no loss if sourced out to separate file.

-----

-----

Can you overcome the (big or not so) implications **TO YOU ALONE**
accompanying this change this **\_one\_ time**? Please let us know:
[VarNames/Vote](VarNames/Vote)!
