The **primary idea** is to **prefix all vars** with a common category
name while **also renaming *some*** main names.

Secondarily, add "\`use\_\`" for all editable vars/ optional features
(example: \`use\_from, use\_my\_hdr\`). Add "\`\_cmd\`" suffix for all
external programs being called.

However, some existing vars already have a proper prefix matching their
category. In the case that some vars a properly prefixed (and others are
not), assume the prefixed ones will NOT get an additional prefix, there
will be only 1 prefix.

Example: "attach\_split" won't become "attach\_attach\_split", even
though it isn't explicitly marked for a rename to "split" only, and then
again to be prefixed with "attach" for the whole
category.

* Add any missing vars.  
* Integrate new vars in the "new" section from 1.5.15-1.5.19~tip.  
* Add/integrate new vars >1.5.19~tip as they come out.  
* For the above, create new sections as necessary and keep the version numbers up to date.  
* Move variables around the sections if you find a better place.  
* If you suggest a name change like "spoolfile" -> "inbox", then **add** the new name in "()" after the original entry.

-----

    system
            alias_file
            check_new (new_while_open)
            dotlock_program (dotlock_cmd)
            editor (editor_cmd)
            locale (time_locale)
            mail_check (newmail_mintime)
            mh_purge
            mh_seq_flagged
            mh_seq_replied
            mh_seq_unseen
            query_command (query_cmd)
            shell
            tmpdir
            timeout (newmail_maxtime)
            header_cache
            maildir_header_cache_verify (header_cache_maildir_verify)
            header_cache_pagesize
            message_cachedir
            spam_separator
            use_ipv6
    
    sendmail
            dsn_notify
            dsn_return
            envelope_from_address
            envelope_from (use_envelope_from) (-> renamed in dev, old has become syn)
            use_envelope_from
            sendmail (sendmail_cmd)
            sendmail_wait (wait)
    
    folder
            folder (base, collection)
            spoolfile (inbox)
            mbox_type (create_type)
            confirmappend
            confirmcreate
            delete
            maildir_trash
            mbox (received)
            move (move_read)
            keep_flagged
            postponed
            read_only
            save_empty (keep_empty)
            copy (use_sent)
            record (sent)
            force_name
            save_address
            save_name
            write_bcc
    
    menu
            arrow_cursor
            ascii_chars
            braille_friendly
            help
            mask (file_mask)
            menu_context (menu_indicator_margin)
            menu_move_off
            menu_scroll
            sleep_time
            sort_browser
            sort_alias
            status_on_top
            wait_key
    
    format (replace all suffixes with prefix)
            alias_format (alias)
            attach_format (attach)
            compose_format (compose)
            date_format (date)
            folder_format (folder)
            index_format (index)
            hdr_format (index)
            message_format (message)
            msg_format (message)
            pager_format (pager)
            status_format (status)
    
    index
            mark_old (mark_unread)
            read_inc
            reverse_alias
            score
            score_threshold_delete
            score_threshold_flag
            score_threshold_read
            sort
            status_chars
            to_chars
            write_inc
    
    pager
            allow_ansi
            display_filter
            implicit_autoview (inline_autoview)
            markers
            pager (ext_cmd)
            pager_context
            pager_index_lines
            pager_stop
            prompt_after
            quote_regexp
            smart_wrap
            smileys
            tilde
            weed
            wrap_search
            wrapmargin
    
    header
            followup_to
            forward_format
            forw_format (forward_format)
            from
            gecos_mask
            hdrs (use_my_hdr)
            hidden_host
            hostname
            realname
            reply_regexp (-> make it 1st, last, only _documented_ synonym for thread_)
            reverse_name
            reverse_realname
            use_domain (use_hostname)
            use_from
            user_agent
    
    body
            attribution
            content_type
            encode_from
            forward_quote
            forw_quote (forward_quote)
            indent_string
            indent_str (indent_string)
            post_indent_string (post_quote)
            post_indent_str (post_quote)
            sig_dashes
            signature
            text_flowed
    
    compose
            abort_nosubject
            abort_unmodified
            askbcc
            askcc
            autoedit
            bounce
            bounce_delivered
            edit_headers
            edit_hdrs (edit_headers)
            fast_reply
            forward_edit
            header (include_header)
            honor_followup_to (use_followup_to)
            ignore_list_reply_to
            include (include_body)
            metoo
            postpone
            recall
            reply_self
            reply_to
    
    thread
            collapse_unread
            duplicate_threads (duplicates)
            hide_limited
            hide_missing
            hide_thread_subject (hide_subject)
            hide_top_limited
            hide_top_missing
            narrow_tree
            reply_regexp (-> make it 1st, last, only _documented_ synonym with header_)
            sort_aux (sort)
            sort_re (partial_re)
            strict_threads (strict)
            thread_received (date_received : isn't this covered by sort_aux?)
            uncollapse_jump
    
    attach
            attach_sep (separator)
            attach_split
            digest_collapse
            fcc_attach (fcc)
            forward_decode
            forw_decode (forward_decode)
            include_onlyfirst
            mailcap_path
            mailcap_sanitize
            mime_forward
            mime_fwd (mime_forward)
            mime_forward_decode
            mime_forward_rest
            pipe_decode
            print_decode
            thorough_search (search_decode)
            rfc2047_parameters
    
    action
            auto_tag
            delete_untag
            quit
            resolve (goto_next)
            suspend
            pipe_sep (pipe_separator)
            pipe_split
            print
            print_command (print_cmd)
            print_cmd (print_cmd)
            print_split
            simple_search
    
    charset
            allow_8bit
            charset (display)
            config_charset (configfile)
            send_charset (send)
            use_8bitmime
            use_idn
    
    crypt
            crypt_autoencrypt
            pgp_autoencrypt (crypt_autoencrypt)
            crypt_autopgp
            crypt_autosign
            pgp_autosign (crypt_autosign)
            crypt_autosmime
            crypt_replyencrypt
            pgp_replyencrypt (crypt_replyencrypt)
            crypt_replysign
            pgp_replysign (crypt_replysign)
            crypt_replysignencrypted (crypt_replysignencrypt)
            pgp_replysignencrypted (crypt_replysignencrypt)
            crypt_timestamp
            crypt_use_gpgme
            crypt_verify_sig
            pgp_verify_sig (crypt_verify_sig)
            fcc_clear
            forward_decrypt
            forw_decrypt (forward_decrypt)
            smime_is_default
    
    pgp
            pgp_check_exit
            pgp_entry_format
            pgp_good_sign
            pgp_ignore_subkeys
            pgp_long_ids
            pgp_retainable_sigs
            pgp_show_unusable
            pgp_sign_as
            pgp_sort_keys
            pgp_strict_enc
            pgp_timeout
            pgp_use_gpg_agent
    
            pgp_mime_auto
            pgp_auto_decode
            pgp_autoinline
            pgp_create_traditional (pgp_autoinline)
            pgp_replyinline
            pgp_auto_traditional (pgp_replyinline)
    
    pgp_cmd (drop all suffixes in favour of prefix)
            pgp_clearsign_command (clearsign)
            pgp_decode_command (decode)
            pgp_decrypt_command (decrypt)
            pgp_encrypt_only_command (encrypt_only)
            pgp_encrypt_sign_command (encrypt_sign)
            pgp_export_command (export)
            pgp_getkeys_command (getkeys)
            pgp_import_command (import)
            pgp_list_pubring_command (list_pubring)
            pgp_list_secring_command (list_secring)
            pgp_sign_command (sign)
            pgp_verify_command (verify)
            pgp_verify_key_command (verify_key)
    
    smime
            smime_ask_cert_label
            smime_decrypt_use_default_key
            smime_timeout
            smime_encrypt_with
            smime_keys
            smime_ca_location
            smime_certificates
            smime_default_key
            smime_sign_as (smime_default_key)
    
    smime_cmd (drop all suffixes in favour of prefix)
            smime_decrypt_command (decrypt)
            smime_verify_command (verify)
            smime_verify_opaque_command (verify_opaque)
            smime_sign_command (sign)
            smime_sign_opaque_command (sign_opaque)
            smime_encrypt_command (encrypt)
            smime_pk7out_command (pk7out)
            smime_get_cert_command (get_cert)
            smime_get_signer_cert_command (get_signer_cert)
            smime_import_cert_command (import_cert)
            smime_get_cert_email_command (get_cert_email)
    
    remote
            connect_timeout
            net_inc
            preconnect (preconnect_cmd)
            tunnel (tunnel_cmd)
    
    imap
            imap_authenticators
            imap_check_subscribed
            imap_delim_chars
            imap_headers
            imap_home_namespace
            imap_idle
            imap_keepalive
            imap_list_subscribed
            imap_login
            imap_pass
            imap_passive
            imap_peek
            imap_servernoise
            imap_user
    
    pop
            pop_auth_try_all
            pop_authenticators
            pop_checkinterval (mail_check)
            pop_delete
            pop_host
            pop_last
            pop_pass
            pop_reconnect
            pop_user
    
    ssl
            certificate_file (cert_file)
            entropy_file
            ssl_ca_certificates_file (ca_cert_file)
            ssl_client_cert
            ssl_force_tls
            ssl_min_dh_prime_bits
            ssl_starttls
            imap_force_ssl (ssl_force_tls)
            ssl_use_sslv2
            ssl_use_sslv3
            ssl_use_tlsv1
            ssl_usesystemcerts
    
    misc
            beep
            beep_new
            default_hook
            ispell (ispell_cmd)
            mix_entry_format
            mixmaster
            sig_on_top
            visual
            escape
            history
            meta_key
    
    new (1.5.15-1.5.19~tip)
        1.5.15
            assumed_charset
            attach_charset
            check_mbox_size
            crypt_use_pka
            history_file
            ignore_linear_white_space
            save_history
            smtp_url
            wrap
        1.5.16
            message_cache_clean
            smtp_pass
        1.5.18
            query_format
            time_inc
        1.5.19
            imap_pipeline_depth
            ssl_client_cert
        "tip"/1.5.20(?)
            honor_disposition
            search_context
            ssl_verify_dates
            ssl_verify_hostname
